public class Java {
	
	public static void main(String args[]) {
		
		int[] lista = new int[10];
		int ceros = 0, nocero = 0;
		
		lista[0] = 0;
		lista[1] = 1;
		lista[2] = 2;
		lista[3] = 3;
		lista[4] = 4;
		lista[5] = 5;
		lista[6] = 6;
		lista[7] = 7;
		lista[8] = 8;
		lista[9] = 9;
		
		
		System.out.println("Lista sin modificar");
		for(int v :lista) {
			System.out.println(v);
		}
		
		System.out.println("Lista con las posiciones pares igual a 0");
		for(int i = 0; i < 10; i++) {
			if(lista[i] % 2 == 0) {
				lista[i] = 0;
			}
			System.out.println(lista[i]);
		}
		
		System.out.println("N�mero de ceros en la lista");
		for(int i = 0; i < 10; i++) {
			if(lista[i] == 0) {
				ceros++;
			}
		}
		System.out.println(ceros);
		
		System.out.println("N�mero de elementos distintos a cero en la lista");
		for(int i = 0; i < 10; i++) {
			if(lista[i] != 0) {
				nocero++;
			}
		}
		System.out.println(nocero);
	}
	
}
